import logging
from logging.handlers import SysLogHandler
import os
import shutil
import sys
import unittest

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))
import pylibs


class SimpleDaemon(pylibs.Daemon):
    def run(self):
        print('Echo!')


class TestDaemon(unittest.TestCase):
    def setUp(self):
        self.test_dir = os.path.join(os.getcwd(), 'unit-test')
        if not os.path.exists(self.test_dir):
            os.mkdir(self.test_dir)

        self.prog_name = 'test-daemon'
        self.logger = pylibs.Logger(self.prog_name, logging.DEBUG,
                                    SysLogHandler.LOG_SYSLOG)

        self.pid_file = os.path.join(self.test_dir, self.prog_name + '.pid')

    def tearDown(self):
        shutil.rmtree(self.test_dir)

    def test_base_daemon(self):
        assert_occured = False
        try:
            daemon = pylibs.Daemon(self.pid_file, prog_name=self.prog_name)
            daemon.start()
        except TypeError:
            assert_occured = True
        except RuntimeError:
            assert_occured = True

        self.assertTrue(assert_occured)

    def test_no_logger(self):
        assert_occured = False
        try:
            daemon = SimpleDaemon(self.pid_file, prog_name=self.prog_name)
            daemon.start()
        except RuntimeError:
            assert_occured = True

        self.assertTrue(assert_occured)

    def test_with_logger_no_detach(self):
        daemon = SimpleDaemon(self.pid_file, prog_name=self.prog_name,
                              logger=self.logger)
        daemon.start(False)
        self.assertEqual(daemon.status(), 0)


if __name__ == '__main__':
    suite = unittest.TestLoader().\
        loadTestsFromTestCase(TestDaemon)
    unittest.TextTestRunner(verbosity=2).run(suite)
