import abc
import atexit
import errno
import os
import pwd
import signal
import subprocess
import sys
import time


_PY3 = False
version = getattr(sys, 'version_info', (0,))
if version >= (3, 0):
    ABC = abc.ABC
    _PY3 = True
else:
    class ABC(object):
        """
        A generic daemon class.

        Usage: subclass the Daemon class and override the run() method
        """
        __metaclass__ = abc.ABCMeta
        pass


class LoggerFile:
    """
    A class for a writable file-like object that calls a function for
    every line of text.  It is intended for replacing stdout/stderr
    and calling a logging function.  A callable object should be
    provided for log_fn.
    """
    def __init__(self, logger, source, name=None):
        self.source = source
        self.logger = logger
        self.name = name
        self._line = ''
        self.softspace = 0

    def __repr__(self):
        if self.name:
            return '<LoggerFile %r>' % self.name
        else:
            return '<LoggerFile at %#x' % id(self)

    def write(self, s):
        while s:
            i = s.find('\n')
            if i < 0:
                self._line += s
                break

            self._line += s[:i]
            if self._line:
                self.logger.info(self._line)
                self._line = ''

            s = s[i + 1:]

    def writelines(self, lines):
        map(self.write, lines)

    def flush(self):
        if self._line:
            self.logger.info(self._line)
            self._line = ''


class Daemon(ABC):
    """
    A generic daemon class.

    Usage: subclass the Daemon class and override the run() method
    """
    def __init__(self, pid_file, logger=None, username=None, prog_name=None):
        self.pid_file = pid_file
        self.logger = logger
        self.username = username
        self.prog_name = prog_name
        self.pid = None

        # Check if the logger already has a handler
        if (logger is None or (_PY3 and not logger.hasHandlers()) or
           len(logger.handlers) == 0):
            raise RuntimeError("Logger doesn't exist or does not have "
                               "any handler attached to it")

    def daemonize(self, detach):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        if detach:
            try:
                pid = os.fork()
                if pid > 0:
                    # exit first parent
                    sys.exit(0)
            except OSError as e:
                sys.stderr.write('fork #1 failed: {0} '
                                 '({1})\n'.format(e.errno, e.strerror))
                sys.exit(1)

            # First child process
            # decouple from parent environment
            os.chdir('/')
            os.setsid()
            os.umask(0)

            # do second fork
            try:
                pid = os.fork()
                if pid > 0:
                    # exit from second parent
                    sys.exit(0)
            except OSError as e:
                sys.stderr.write('fork #2 failed: {0} '
                                 '({1})\n'.format(e.errno, e.strerror))
                sys.exit(1)

            # Second child process
            # Change the uid, assuming a username has been specified
            if self.username:
                try:
                    uid = pwd.getpwnam(self.username).pw_uid
                    os.setuid(uid)
                except (KeyError, OSError) as e:
                    sys.stderr.write('Problem with setting user id to {0}: '
                                     '{1}\n'.format(self.username, e))
                    sys.exit(1)

            # redirect standard file descriptors
            sys.stdout.write('Started daemon, PID ({0})\n'.format(os.getpid()))
            sys.stdout.flush()
            sys.stderr.flush()

            # Close stdin, stdout, stderr, and reopen pointing to /dev/null.
            # This assumes these are the first three file descriptors as is
            # usually the case.  Bad things could happen if these were altered
            # before this point.
            for fd in range(0, 3):
                try:
                    os.close(fd)
                except:
                    pass

            os.open('/dev/null', os.O_RDONLY)
            os.open('/dev/null', os.O_WRONLY)
            os.open('/dev/null', os.O_WRONLY)
            sys.stdout = LoggerFile(self.logger, '<stdout>')
            sys.stderr = LoggerFile(self.logger, '<stderr>')

        # write pidfile
        (self.pid, err) = lock_file(self.pid_file)
        if err:
            sys.stderr.write('pidfile {0} cannot be created.  Detail: '
                             '{1}\n'.format(self.pid_file, err))
            sys.exit(1)

        # Delete the pid file at exit
        atexit.register(self.del_pid)

    def del_pid(self):
        # Only remove the pid file if the deamon itself, and not one of it
        # child processes, is exiting
        if self.pid == os.getpid():
            try:
                os.remove(self.pid_file)
            except OSError as e:
                sys.stderr.write('Unable to remove pidfile {0}: {1} - '
                                 '{2}\n'.format(self.pid_file, e.errno,
                                                e.strerror))

    def start(self, detach=True):
        """
        Start the daemon
        """
        # Check for a pidfile to see if the daemon already runs
        (pid, err) = is_daemon_running(self.pid_file, self.prog_name)
        if err:
            sys.stderr.write('pidfile {0} cannot be accessed.  Detail: '
                             '{1}\n'.format(self.pid_file, err))
            sys.exit(1)
        if pid != 0:
            sys.stderr.write('Daemon already running, PID ({0})\n'.format(pid))
            sys.exit(1)

        # Start the daemon
        self.daemonize(detach)
        self.run()

    def stop(self):
        """
        Stop the daemon
        """
        # Get the pid from the pidfile
        rc = 0
        (pid, err) = is_daemon_running(self.pid_file, self.prog_name)
        if err:
            sys.stderr.write('Cannot access pidfile {0}, '
                             '{1}\n'.format(self.pid_file, err))
            rc = 1

        if pid == 0:
            sys.stderr.write('Daemon is not running\n')
            rc = 1
        else:
            # Try killing the daemon process
            try:
                os.kill(pid, signal.SIGTERM)
                (pid, err) = is_daemon_running(self.pid_file, self.prog_name)
                while pid != 0:
                    time.sleep(1)
                    (pid, err) = is_daemon_running(self.pid_file,
                                                   self.prog_name)
            except OSError as e:
                if e.errno != errno.ESRCH:
                    sys.stderr.write('Unable to stop the running process {0}, '
                                     '{1}\n'.format(e.errno, e.strerror))
                    rc = 1

            try:
                if rc == 0 and pid == 0 and os.path.exists(self.pid_file):
                    os.remove(self.pid_file)
            except OSError as e:
                # Maybe we cannot remove the file, but we have stopped the
                # daemon, so all is good
                sys.stderr.write('Unable to remove pidfile {0}, {1} '
                                 '({2})\n'.format(self.pid_file, e.errno,
                                                  e.strerror))
                rc = 1

        return rc

    def restart(self):
        """
        Restart the daemon
        """
        self.stop()
        self.start()

    def status(self):
        """
        Check the status of the daemon
        """
        # Get the pid from the pidfile
        rc = 0
        (pid, err) = is_daemon_running(self.pid_file, self.prog_name)
        if err:
            sys.stderr.write('Cannot access pidfile {0}, '
                             '{1}\n'.format(self.pid_file, err))
            rc = 1
        elif pid == 0:
            sys.stdout.write('Daemon is not running\n')
            rc = 1
        else:
            sys.stdout.write('Daemon is running, PID ({0})\n'.format(pid))

        return rc

    def run(self):
        """
        You should override this method when you subclass Daemon. It will be
        called after the process has been daemonized by start() or restart().
        """
        raise RuntimeError('Cannot run the abstract Daemon method')


def is_daemon_running(pid_file, prog_name=None):
    pid = 0
    err = None
    if _PY3:
        devnull = subprocess.DEVNULL
    else:
        devnull = open(os.devnull, 'w')

    try:
        with open(pid_file, 'r') as f:
            # A lock file already exists.  Check if the owner is alive
            pidStr = f.read().strip('\n')
            command = ['ps', '-p', pidStr]

            proc = subprocess.Popen(command, stdout=devnull, stderr=devnull,
                                    universal_newlines=True)
            alive = proc.wait() == 0
            if alive:
                # An Owner already exists
                pid = int(pidStr)
    except IOError as e:
        if e.errno != errno.ENOENT:
            err = e
            pid = -1
    except ValueError as e:
        # File is corrupt, trying to look up by program name
        pid = -1
        err = ''

    if pid in [-1, 0] and prog_name:
        command = ['pgrep', '-P', '1', '-f', prog_name]
        proc = subprocess.Popen(command, stdout=devnull, stderr=devnull,
                                universal_newlines=True)
        alive = proc.wait() == 0
        if alive:
            # At least one owner already exists
            output, error = proc.communicate()
            pidList = filter(lambda x: x != os.getpid(),
                             [int(line) for line in output.split('\n')])
            if len(pidList) > 1:
                pid = -1
                err = 'There are more than one process ID running.  You ' \
                    'need to manually remove the PIDs '\
                    '({0})'.format(', '.join(map(str, pidList)))
            elif len(pidList) == 1:
                pid = pidList[0]

    if not _PY3:
        devnull.close()

    return (pid, err)


def lock_file(filename):
    (pid, err) = is_daemon_running(filename)
    if pid == 0:
        try:
            with open(filename, 'w') as f:
                f.write(str(os.getpid()))
        except IOError as e:
            pid = -1
            err = str(e)

    return pid, err
