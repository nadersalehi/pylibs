"""
Python libraries

Copyright (c) 2015 Nader Salehi
"""
import sys
from .syslog import Logger
from .versions import Version
from .daemon import Daemon, is_daemon_running, lock_file


def _check_requirements():
    # Don't allow the user to run a version of Python we don't support.
    version = getattr(sys, 'version_info', (0,))
    if version < (2, 3):
        raise ImportError('Logger requires Python 2.3 or later.')

    required = '{0}.{1}'.format(version.major, version.minor)
    try:
        import logging
    except ImportError:
        raise ImportError('No module named logging in version '
                          '{0}'.format(required))
    except:
        raise ImportError('System Logger requires logging {0} or '
                          'later'.format(required))

    try:
        import logging.handlers
    except ImportError:
        raise ImportError('No module named logging.handlers in version '
                          '{0}'.format(required))
    except:
        raise ImportError('System Logger requires logging handlers {0} or '
                          'later'.format(required))


_check_requirements()


_PY3 = False
py_version = getattr(sys, 'version_info', (0,))
package_name = 'pylibs'
if 'bdist_rpm' in sys.argv:
    pkg_name = ('python3-' if py_version >= (3, 0) else 'python-') + \
        package_name
else:
    pkg_name = package_name

version = Version(package_name, 0, 0, 1, 1)
__version__ = version.short()
STATIC_PACKAGE_METADATA = dict(name=pkg_name,
                               version=__version__,
                               description='An ad-hoc set of libraries to '
                                           'help me with development',
                               author='Nader Salehi',
                               author_email='nadersalehi1@gmail.com',
                               maintainer='Nader Salehi',
                               maintainer_email='nadersalehi1@gmail.com',
                               license='MIT',
                               long_description="""\
""",
                               classifiers=[
                                   "Programming Language :: Python :: 2.6",
                                   "Programming Language :: Python :: 2.7",
                                   "Programming Language :: Python :: 3",
                                   "Programming Language :: Python :: 3.3",
                                   "Programming Language :: Python :: 3.4",
                               ]
                               )


__all__ = ['Logger',
           'Version',
           '__version__',
           'Daemon',
           'is_daemon_running',
           'lock_file']
