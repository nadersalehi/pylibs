"""
The syslog package

Copyright (c) 2015 Nader Salehi
"""

___all__ = ['Logger']


def _check_requirements():
    # Don't allow the user to run a version of Python we don't support.
    import sys
    version = getattr(sys, 'version_info', (0,))
    if version < (2, 3):
        raise ImportError('Logger requires Python 2.3 or later.')

    required = '{0}.{1}'.format(version.major, version.minor)
    try:
        import logging
    except ImportError:
        raise ImportError('No module named logging in version '
                          '{0}'.format(required))
    except:
        raise ImportError('System Logger requires logging {0} or '
                          'later'.format(required))

    try:
        import logging.handlers
    except ImportError:
        raise ImportError('No module named logging.handlers in version '
                          '{0}'.format(required))
    except:
        raise ImportError('System Logger requires logging handlers {0} or '
                          'later'.format(required))


_check_requirements()


import logging
from logging.handlers import SysLogHandler


class Logger(logging.Logger):
    def __init__(self, name, log_level=logging.INFO,
                 log_facility=SysLogHandler.LOG_SYSLOG,
                 fmt=None,
                 trace_id=0):
        logging.Logger.__init__(self, name)
        self.trace_id = trace_id
        self.fmt = fmt
        if not self.fmt:
            self.fmt = logging.Formatter('%(name)s[%(process)d] '
                                         '%(message)s ')
        self.log_level = log_level
        self.log_facility = log_facility
        self.log_handler = SysLogHandler(address='/dev/log',
                                         facility=self.log_facility)
        self.log_handler.setFormatter(self.fmt)
        self.setLevel(self.log_level)
        self.addHandler(self.log_handler)

    def set_trace_id(self, tid):
        self.trace_id = tid

    def form_message(self, msg, level, *args, **kwargs):
        if 'trace_id' in kwargs:
            tid = kwargs['trace_id']
            del kwargs['trace_id']
        else:
            tid = self.trace_id

        message = '({0}): {1} {2}'.format(tid, logging.getLevelName(level),
                                          msg)
        return message, args, kwargs

    def debug(self, msg, *args, **kwargs):
        self.log(logging.DEBUG, msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.log(logging.INFO, msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        self.log(logging.WARNING, msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.log(logging.ERROR, msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        self.log(logging.CRITICAL, msg, *args, **kwargs)

    def exception(self, msg, *args, **kwargs):
        self.log(logging.ERROR, msg, *args, **kwargs)

    def log(self, level, msg, *args, **kwargs):
        if 'trace_id' in kwargs:
            tid = kwargs['trace_id']
            del kwargs['trace_id']
        else:
            tid = self.trace_id

        message = '({0}): {1} {2}'.format(tid, logging.getLevelName(level),
                                          msg)
        super(Logger, self).log(level, message, *args, **kwargs)
