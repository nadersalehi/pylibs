# -*- test-case-name: twisted.python.test.test_versions -*-
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.

"""
Versions for Python packages.

See L{Version}.
"""

from __future__ import division, absolute_import
import re
import subprocess
from .compat import cmp, comparable


@comparable
class _inf(object):
    """
    An object that is bigger than all other objects.
    """
    def __cmp__(self, other):
        """
        @param other: Another object.
        @type other: any

        @return: 0 if other is inf, 1 otherwise.
        @rtype: C{int}
        """
        if other is _inf:
            return 0
        return 1

_inf = _inf()


class IncomparableVersions(TypeError):
    """
    Two versions could not be compared.
    """


@comparable
class Version(object):
    """
    An object that represents a three-part version number.

    If running from an svn checkout, include the revision number in
    the version string.
    """
    def __init__(self, package, major, minor, bugfix, prerelease=None):
        """
        @param package: Name of the package that this is a version of.
        @type package: C{str}
        @param major: The major version number.
        @type major: C{int}
        @param minor: The minor version number.
        @type minor: C{int}
        @param bugfix: The bugfix version number.
        @type bugfix: C{int}
        @param prerelease: The prerelease number.
        @type prerelease: C{int}
        """
        self.package = package
        self.major = major
        self.minor = minor
        self.bugfix = bugfix
        self.prerelease = prerelease

    def short(self):
        """
        Return a string in canonical short version format,
        <major>.<minor>.<bugfix>[-rSVNVer].
        """
        s = self.base()
        svnver = self._get_svn_version()
        if svnver:
            s += '-r' + svnver
        return s

    def base(self):
        """
        Like L{short}, but without the -rSVNVer.
        """
        if self.prerelease is None:
            pre = ''
        else:
            pre = 'pre%s' % (self.prerelease,)
        return '%d.%d.%d%s' % (self.major,
                               self.minor,
                               self.bugfix,
                               pre)

    def __repr__(self):
        svnver = self._format_svn_version()
        if svnver:
            svnver = '  #' + svnver
        if self.prerelease is None:
            prerelease = ''
        else:
            prerelease = ', prerelease=%r' % (self.prerelease,)
        return '%s(%r, %d, %d, %d%s)%s' % (
            self.__class__.__name__,
            self.package,
            self.major,
            self.minor,
            self.bugfix,
            prerelease,
            svnver)

    def __str__(self):
        return '[%s, version %s]' % (
            self.package,
            self.short())

    def __cmp__(self, other):
        """
        Compare two versions, considering major versions, minor versions,
        bugfix versions, then prereleases.

        A version with a prerelease is always less than a version without a
        prerelease. If both versions have prereleases, they will be included in
        the comparison.

        @param other: Another version.
        @type other: L{Version}

        @return: NotImplemented when the other object is not a Version, or one
            of -1, 0, or 1.

        @raise IncomparableVersions: when the package names of the versions
            differ.
        """
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.package != other.package:
            raise IncomparableVersions('%r != %r'
                                       % (self.package, other.package))

        if self.prerelease is None:
            prerelease = _inf
        else:
            prerelease = self.prerelease

        if other.prerelease is None:
            otherpre = _inf
        else:
            otherpre = other.prerelease

        x = cmp((self.major,
                 self.minor,
                 self.bugfix,
                 prerelease),
                (other.major,
                 other.minor,
                 other.bugfix,
                 otherpre))
        return x

    def _get_svn_version(self):
        """
        Figure out the SVN revision number based on the existence of
        <package>/.svn/entries, and its contents. This requires discovering
        the format version from the 'format' file and parsing the entries file
        accordingly.

        @return: None or string containing SVN Revision number.
        """
        svnver = None
        p = subprocess.Popen('svn info'.split(),
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             universal_newlines=True)
        rc = p.wait()
        if rc == 0:
            stdout, stderr = p.communicate()
            revision_regex = re.compile('^Revision: (\d+)')
            match = list(filter(lambda x: x is not None,
                                map(revision_regex.search,
                                    stdout.split('\n'))))
            if len(match) != 0:
                svnver = match[0].group(1)

        return svnver

    def _format_svn_version(self):
        ver = self._get_svn_version()
        if ver is None:
            return ''
        return ' (SVN r%s)' % (ver,)


def get_version_string(version):
    """
    Get a friendly string for the given version object.

    @param version: A L{Version} object.
    @return: A string containing the package and short version number.
    """
    result = '%s %s' % (version.package, version.short())
    return result
