#!/usr/bin/env python

# Copyright (c) Nader Salehi.

"""
Installer for Python Utility Library
"""

# Load setuptools, to build a specific source package
import os
import setuptools
import sys


class clean_command(setuptools.Command):
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system('rm -vrf ./build ./dist ./*.pyc ./*.tgz ./*.egg-info')


def main(args):
    """
    Invoke installer with the appropriate metadata about the package
    """
    if os.path.exists('pylibs'):
        sys.path.insert(0, '.')

    from pylibs import STATIC_PACKAGE_METADATA

    setup_args = {}
    setup_args.update(dict(packages=setuptools.find_packages(),
                           cmdclass={'clean': clean_command},
                           **STATIC_PACKAGE_METADATA))
    setuptools.setup(**setup_args)


if __name__ == '__main__':
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        sys.exit(1)
